package views;

import festival.FestivalCommand;

@:build(haxe.ui.ComponentBuilder.build("assets/layout/log-component.xml"))
class LogComponent extends haxe.ui.containers.VBox {

    public function new(){
        super();
    }

   @:bind(sendCommand, haxe.ui.events.UIEvent.SUBMIT)
	function sendUserCommand(e){
        FestivalCommand.sendLispCommand(sendCommand.text);
		sendCommand.text = "";

	}
    
}