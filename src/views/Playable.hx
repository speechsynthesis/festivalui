package views;

import haxe.io.Bytes;

interface Playable {

    public var possibleTags : Map<String, Array<String>>;

    public function play(s:String):Void;
    public function stopText():Void;
    public function saveWave(savePath:String = null):Bytes;

    public function onShow():Void; 

}