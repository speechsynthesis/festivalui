package views;

import haxe.ui.containers.VBox;
import sys.io.Process;

@:xml(
'
<vbox width="100%" height="50%" padding="8">
<frame id="warning" text="">
    <label id="warning_text" color="#FF0000" />
</frame>

<vbox id="params_box">

<dropdown text="{{LANGUAGE}}" id="language">
    <data>
        <item text= "en-US" />
        <item text= "en-GB" />
        <item text= "de-DE" />
        <item text= "es-ES" />
        <item text= "fr-FR" />
        <item text= "it-IT" />
    </data>
</dropdown>
</vbox>
</vbox>'

)
class PicoSayTextComponent extends VBox implements Playable  {

    private var playProcess:Process = null;

    public var possibleTags : Map<String, Array<String>> = [ 
        "ignore" => null,  
        "p"      => null,
        "s"      => null,
        "break"  => ["time"],
        "speed"  => ["level"],
        "volume" => ["volume"] ];

    public function new(text:String, id:String){
        super();
        this.text = text;
        this.id   = id;
        //canWork();
    }

    public function onShow(){
        Config.currentConfigName = text;
    }

	public function play(s){
		var text = s; 
		//sys.io.File.saveContent("myfile.txt", text);
        
        var path = Config.getParameter("path", null);
        var cmd = path;
        Sys.command(cmd, ["-l", language.text, "-w", "/tmp/temp_pico.wav", text]);

        
        //Sys.command(Config.mainPlayer + "/tmp/temp_pico.wav");
        #if (target.threaded)
        sys.thread.Thread.create(() -> {
            playProcess = new Process(Config.mainPlayer + "/tmp/temp_pico.wav");
            playProcess.exitCode(true);
                playProcess.close();
        });
        #end
	}

    @:bind(this,haxe.ui.events.UIEvent.READY)
    public function canWork(e) {
        Config.currentConfigName = this.text; 
        var path = Config.getParameter("path", null);
        warning.hidden = Utils.checkCommand(path);
        warning_text.text = path + " doesn't exist";
        params_box.disabled = !warning.hidden;
    }

	public function stopText(){
        playProcess.kill();
        playProcess.close();


		//FestivalCommand.sendLispCommand('(format t "Stop")');
	}

    public function saveWave(savePath:String = null) {

        var s = rootComponent.findComponent("textBox", haxe.ui.components.TextArea).text;

        var path = Config.getParameter("path", null);
        var cmd = path;
        Sys.command(cmd, ["-l", language.text, "-w", savePath, s]);

        return null;
    }
    
}