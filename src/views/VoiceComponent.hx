package views;

import festival.FestivalCommand;
import festival.LispGrammar;
import haxe.ui.events.UIEvent;

@:build(haxe.ui.ComponentBuilder.build("assets/layout/voice-description.xml"))
class VoiceComponent extends haxe.ui.containers.VBox {
	public function new() {
		super();
	}

	public function startingServer() {
		languagesDropdown.dataSource.add("unattached");
		FestivalCommand.sendLispCommand("(language.list)", function(s:String) {
			FestivalCommand.decodeLispResult(s, languagesDropdown.dataSource.add);
		});
	}

	public function getParameters() {
		var lang = "(voice.select '" + voiceName.text + ") \n";

		var speed = "(Parameter.set 'Duration_Stretch " + voiceStretch.value + ")\n";
		FestivalCommand.sendLispCommand(speed);

		var volume = ' (set! after_synth_hooks
      (list
        (lambda (utt)
          (utt.wave.rescale utt ${volume.value} ))))';

		FestivalCommand.sendLispCommand(volume);

		return lang + speed + volume;
	}

	@:bind(languagesDropdown, UIEvent.CHANGE)
	function checkVoices(e) {
		if (languagesDropdown.selectedItem == "unattached") {
			FestivalCommand.sendLispCommand("(voice.list)", function(s:String) {
				voicesList.dataSource.clear();
				FestivalCommand.decodeLispResult(s, voicesList.dataSource.add);
			});
			return;
		}
		var cmd = "(language.get_voices '" + languagesDropdown.selectedItem + ")";
		FestivalCommand.sendLispCommand(cmd, function(s:String) {
			voicesList.dataSource.clear();

			var v = LispGrammar.build()(s).value;
			var maleE:Array<String> = cast FestivalCommand.convertLisp(FestivalCommand.walkThroughLispExpression(v, [0, 1]));
			if ((maleE != null)) {
				for (a in maleE)
					voicesList.dataSource.add(a);
			}
			var femaleE:Array<String> = cast FestivalCommand.convertLisp(FestivalCommand.walkThroughLispExpression(v, [1, 1]));

			if ((femaleE != null)) {
				for (a in femaleE)
					voicesList.dataSource.add(a);
			}
		});
	}

	@:bind(voicesList, UIEvent.CHANGE)
	function selectVoice(e) {
		if (voicesList.selectedItem != null) {
			FestivalCommand.sendLispCommand("(voice.description '" + voicesList.selectedItem + ")", function(s:String) {
				voiceDescription.text = StringTools.replace(s,"\n","");
				voiceName.text = voicesList.selectedItem;
			});
			FestivalCommand.sendLispCommand("(voice.select '" + voicesList.selectedItem + ")");
		}
	}

	@:bind(voiceStretch, haxe.ui.events.MouseEvent.CLICK)
	function changeVoiceStretch(e) {
		var speed = "(Parameter.set 'Duration_Stretch " + voiceStretch.value + ")";
		FestivalCommand.sendLispCommand(speed);
	}

	@:bind(volume, haxe.ui.events.MouseEvent.CLICK)
	function changeVolume(e) {
		var volume = ' (set! after_synth_hooks
      (list
        (lambda (utt)
          (utt.wave.rescale utt ${volume.value} ))))';

		FestivalCommand.sendLispCommand(volume);
	}
}
