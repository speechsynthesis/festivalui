package views; 
import haxe.ui.events.MouseEvent;


@:xml('
<menubar width="100%">
        <menu text="File">
            <menuitem text="{{PARAMETERS}}" id="parameters" />
        </menu>

    </menubar>
')
class MainMenu extends haxe.ui.containers.menus.MenuBar {

    public function new(){
        super();
    }

    @:bind(parameters, MouseEvent.CLICK)
    public function showParametersDialog(e){
        var d = new ParametersDialog(); 
        d.show();
    }
}
