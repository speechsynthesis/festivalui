package views;

import haxe.ui.containers.dialogs.Dialog.DialogButton;
import haxe.ui.containers.dialogs.SaveFileDialog;
import haxe.ui.containers.dialogs.Dialogs.FileDialogTypes;
import haxe.ui.containers.HBox;
import haxe.ui.containers.ContinuousHBox;
import haxe.ui.containers.HBox;
import haxe.ui.containers.HBox;
import haxe.ui.containers.VBox;
import haxe.ui.components.TextField;
import haxe.ui.containers.menus.*;
import haxe.ui.events.MouseEvent;
import haxe.ui.events.FocusEvent;
import haxe.ui.events.UIEvent;
import haxe.ui.components.Button;

using StringTools;

@:xml("
<vbox >
<style>
.highlight {
    color: green;
    //background-color: #0000FF;
}

.highlight .menuitem-label {
    color: green;
}
</style>

<textarea id='textBox' width= '100%' height='100%' placeholder='Tapez le texte que vous voulez faire lire'/>
<hbox width='100%'>
    <button text  = '{{PLAY}}' id='play'/>
    <button text  = '{{STOP}}' id='stop'/>
    <button text  = '{{SAVE_WAVE}}' id='save_wave'/>
</hbox>
</vbox>

"
)

class TextComponent extends VBox {

    var selection = null;
    
    var ssmlTags : Map<String, Array<String>> = [ 
        "ignore" => null,  
        "p"      => null,
        "s"      => null,
        "break"  => ["time"],
        "speed"  => ["level"],
        "volume" => ["level"] ];
    
    public function new() {
        super ();   
    }

    private function selectedElement() {
        var beg = selection.beg;
        var end = selection.end;
        var selectedText:String =  selection.text;

        if ( selectedText.trim().startsWith("<") && 
                  selectedText.trim().endsWith(">")) { 
            return Xml.parse(selectedText) ;} 
        else {
            return Xml.createPCData(selectedText);
        }
    }
    

    /*
    @:bind(textBox, MouseEvent.CLICK)
    private function showRealText(e){
        #if openfl
        textBox.getTextInput().textField.text = textBox.text;
         #end
    }*/


    @:bind(textBox, MouseEvent.RIGHT_CLICK)
    private function openMenu(e) {
        var event:MouseEvent = cast e;
        event.cancel();
        var menu:Menu = new Menu(); 

        var subMenu:Menu = new Menu(); 
         subMenu.text = "add ssml tag"; ////"{{ADD_SSML_TAG}}";
         menu.addComponent(subMenu);

        var begInd = textBox.getTextInput().selectionStartIndex;
        var endInd = textBox.getTextInput().selectionEndIndex;
        var selectedText = textBox.text.substring(begInd, endInd);
        selection = { beg:begInd, end:endInd,  text:selectedText, component:textBox }

        var view:Playable = cast rootComponent.findComponent("tabs", haxe.ui.containers.TabView).selectedPage;
        var possibleTags = view.possibleTags;

        for (tag => params in SSMLTags.allTags) {
            var menuitem = new MenuItem();
            menuitem.text = tag;
            menuitem.userData = params;
            if (possibleTags.exists(tag)) {
                menuitem.addClass("highlight");
            }

            subMenu.addComponent(menuitem);
        }

        menu.onMenuSelected = function(e) {

            var tag = e.menuItem.text;
            var params = e.menuItem.userData;
            var tagElement = Xml.createElement(tag);
                    var beg:Int = begInd;
                    var end:Int = endInd;
                    tagElement.addChild(selectedElement());
                if (params == null) {
                    textBox.text = textBox.text.substring(0, beg) + tagElement + textBox.text.substring(end);
                }
                else {
                    var possParams: Array<String> = [];
                    if (possibleTags.exists(tag)) {
                        possParams = possibleTags[tag];
                    }

                    var dialog = new SSMLTagsDialog(params, possParams);
                    dialog.title = tag;
                    
                    dialog.beg = beg;
                    dialog.end = end;
                    dialog.textBox = textBox;
                    dialog.tagElement =  tagElement;

                    dialog.show();
                }
        }
        
		menu.left = event.screenX;
		menu.top  = event.screenY;
        menu.show();
    }

    private var playedView:Playable = null;

    @:bind(play, haxe.ui.events.MouseEvent.CLICK)
	function playText(e){
		var view:Playable = cast rootComponent.findComponent("tabs", haxe.ui.containers.TabView).selectedPage;
        view.play(textBox.text);
        playedView = view;
		
	}

	@:bind(stop, haxe.ui.events.MouseEvent.CLICK)
	function stopText(e){
        if (playedView == null) return;
        playedView = cast rootComponent.findComponent("tabs", haxe.ui.containers.TabView).selectedPage;
        playedView.stopText();
	}

    @:bind(save_wave, haxe.ui.events.MouseEvent.CLICK)
	function saveWave(e){
        
        var dialog = new SaveFileDialog(
            
            function f(button, result, fullPath) {
            var view:Playable = cast rootComponent.findComponent("tabs", haxe.ui.containers.TabView).selectedPage;
            view.saveWave(fullPath);
        });
        
        dialog.options = {
            title: "Save Sound File",
            extensions: FileDialogTypes.ANY
        }
        
        dialog.fileInfo = {
            name: "TTS.wav",
            bytes: null
        }
        dialog.show();
	}


}