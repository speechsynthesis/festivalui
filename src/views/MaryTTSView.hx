package views;

import haxe.ui.containers.VBox;
import sys.io.Process;
@:xml('
<vbox width="100%" height="100%" padding="8">
<vbox width="100%" height="50%">
<spacer height="10"/>
<frame id="warning" text="">
    <label id="warning_text" color="0xFF0000" />
</frame>
</vbox>
<vbox id="params_box" >
    <hbox>
        <listview text="{{LANGUAGES}}" id="languagesDropdown" width="200" height="150">
            <data>
            </data>
        </listview>
        <listview text="{{VOICES}}" id="voicesDropdown" width="200" height="150">
            <data>
            </data>
        </listview >
    </hbox>
    </vbox>
</vbox>
')
class MaryTTSView extends VBox implements Playable {
	private var playProcess:Process = null;

	public var possibleTags:Map<String, Array<String>> = [];

	public var voices:Array<Array<String>> = new Array();

	public function new() {
		super();
	}

	public function onShow() {
		Config.currentConfigName = text;
		var url = "http://" + Config.getParameter("host", "localhost") + ":" + Config.getParameter("port", "59625") + "/voices";
		var h = new haxe.Http(url);
		h.onData = function(data) {
			warning.hidden = true;
			for (l in data.split("\n")) {
				var v = l.split(" ");
				voices.push(v);
				if (languagesDropdown.dataSource.indexOf(v[1]) == -1) {
					languagesDropdown.dataSource.add(v[1]);
				}
			}
		}
		h.onError = function(error) {
			warning.hidden = false;
			warning_text.text = "problem connecting to " + url;
			params_box.disabled = !warning.hidden;
		}
		h.request();
	}

	@:bind(languagesDropdown, haxe.ui.events.UIEvent.CHANGE)
	private function showVoices(e) {
		voicesDropdown.dataSource.clear();
		for (v in voices) {
			if (languagesDropdown.selectedItem == v[1]) {
				voicesDropdown.dataSource.add(v[0]);
			}
		}
	}

	private function urlForMary(s:String) {
		var url = "http://" + Config.getParameter("host", "localhost") + ":" + Config.getParameter("port", "59625") + "/process?";
		var t = StringTools.htmlEscape(s);
		t = StringTools.replace(t, " ", "+");
		url += "INPUT_TEXT=" + t;
		url += "&INPUT_TYPE=TEXT&OUTPUT_TYPE=AUDIO&AUDIO=WAVE_FILE";
		url += '&LOCALE=' + languagesDropdown.selectedItem;

		if (voicesDropdown.selectedIndex >= 0) {
			url += "&VOICE=" + voicesDropdown.selectedItem;
		}

		return url;


	}

	public function play(s:String) {
		var url = urlForMary(s);
		
		var h = new haxe.Http(url);
		h.onData = function(data) {
			sys.io.File.saveBytes("/tmp/waveToRead.wav", haxe.io.Bytes.ofString(data));
			// Sys.command(Config.mainPlayer + " /tmp/waveToRead.wav");
			#if (target.threaded)
			sys.thread.Thread.create(() -> {
				playProcess = new Process(Config.mainPlayer + " /tmp/waveToRead.wav");
				playProcess.exitCode(true);
				playProcess.close();
			});
			#end
		}

		h.request();
	}

	public function stopText() {
		if (playProcess != null) {
			Sys.command("kill " + playProcess.getPid());
			playProcess.kill();
			playProcess.close();
		}
	}

	public function saveWave(savePath:String = null) {

		var s = rootComponent.findComponent("textBox", haxe.ui.components.TextArea).text;

		var url = urlForMary(s);
		
		var h = new haxe.Http(url);
		h.onData = function(data) {
			sys.io.File.saveBytes(savePath, haxe.io.Bytes.ofString(data));
		}

        return null;
    }
}
