package views;

import haxe.ui.components.TextArea;
import haxe.ui.util.Timer;
import haxe.ui.containers.HBox;
import festival.FestivalCommand;

@:build(haxe.ui.ComponentBuilder.build("assets/layout/festival-view.xml"))
class FestivalView extends HBox implements Playable {

    public var possibleTags : Map<String, Array<String>> = [];

    public var isStarted:Bool = false;

    public function new(text:String,id:String){
        super();
        this.text = text;
        this.id = id;

    }

    private function startingServer(){
        Config.currentConfigName = this.text;
        FestivalCommand.startFestivalServer();
        Timer.delay(voiceDescription.startingServer, 1000);
        
    }

    public function onShow(){
        Config.currentConfigName = this.text;
        if (!isStarted) {
            startingServer();
            isStarted = true;
        }
    }

    public function play(s:String) {
        var param = parentComponent.findComponent("voiceDescription", VoiceComponent).getParameters();
		var sayText = '(SayText "'+ s +'")';
        FestivalCommand.sendLispCommand(param + sayText);

    }

    public function stopText(){
		//FestivalCommand.sendLispCommand('(format t "Stop")');
	}

    @:bind(this,haxe.ui.events.UIEvent.READY)
    public function canWork(e) {
        Config.currentConfigName = this.text; 
        var path = Config.getParameter("festivalServerPath", null);
        warning.hidden = Utils.checkCommand(path);
        warning_text.text = path + " doesn't exist";
        voiceDescription.disabled = !warning.hidden;
        logView.disabled = !warning.hidden;
    }

    public function saveWave(savePath:String = null) {
        // it saves the wave directly
        var s = rootComponent.findComponent("textBox", TextArea).text;

        var param = parentComponent.findComponent("voiceDescription", VoiceComponent).getParameters();
		var sayText =  '(utt.save.wave (utt.synth (eval (list \'Utterance \'Text "$s"))) "$savePath")';
        FestivalCommand.sendLispCommand(param + sayText);

        return null;
    }
}
