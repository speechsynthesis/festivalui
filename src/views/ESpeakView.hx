package views;

import haxe.ui.containers.menus.*;
import haxe.ui.core.Screen;
import haxe.ui.containers.VBox;

import sys.io.Process;

@:xml('
<vbox width="100%" height="100%" padding="8">
<frame id="warning" text="">
    <label id="warning_text" color="0xFF0000" />
</frame>
<vbox id="params_box">
    <hbox>
        <listview text="{{LANGUAGES}}" id="languagesDropdown" width="200" height="150">
            <data>
            </data>
        </listview>
        <listview text="{{VARIANTS}}" id="variantDropdown" width="200" height="150">
            <data>
                <item text="default" />
                <item text="m1" />
                <item text="m2" />
                <item text="m3" />
                <item text="m4" />
                <item text="m5" />
                <item text="m6" />
                <item text="m7" />
                <item text="f1" />
                <item text="f2" />
                <item text="f3" />
                <item text="f4" />
                <item text="f5" />
                <item text="whisper" />
                <item text="croak" />
                <item text="old" />
                <item text="young" />
            </data>
        </listview >
    <vbox>
    <hbox>
    <label text="{{VOLUME}}" />
    <slider min="0" max="200" pos="100"  center="100" id="volume"/>
    </hbox>
    <hbox>
    <label text="{{PITCH}}" />
    <slider min="0" max="99" pos="50"  center="50" id="pitch"/>
    </hbox>
    <hbox>
    <label text="{{SPEED}}" />
    <slider min="80" max="300" pos="175"  center="175" id="speed"/>
    </hbox>
    <hbox>
    <label text="{{GAPWORDS}}" />
    <slider min="0" max="100" pos="10"  center="10" id="gap"/>
    </hbox>
    <button toggle="true" selected="true" text="{{SSMLMODE}}" id="ssml"/>
    </vbox>
    </hbox>
    </vbox>
        
</vbox>


')
class ESpeakView extends VBox implements Playable  {

    private var playProcess:Process = null;

    public var possibleTags : Map<String, Array<String>> = [ 
        "ignore" => null,  
        "p"      => null,
        "s"      => null,
        "break"  => ["time", "strength" ],
        "speed"  => ["level"],
        "volume" => ["volume" ]];

    public var hasInitialised = false;

    public function new(text:String, id:String){
        super();
        this.text = text;
        this.id   = id;
        Config.currentConfigName = this.text;  
        var path = Config.getParameter("path", null);
        warning.hidden = !Utils.checkCommand(path);
    }

    public function onShow(){
        Config.currentConfigName = this.text;     
        extractVoices(); 
    }


    public function extractVoices(){
        if ( hasInitialised) return;
        var process = new sys.io.Process ("espeak --voices");
        var voices = process.stdout.readAll().toString();
        var voicesLines:Array<String> = voices.split("\n");
         var str = "XaaaYababZbbbW";
        var r = ~/\s+/g;
        //var ds = new haxe.ui.data.DataSource<Dynamic>();
        for (v in voicesLines.slice(1) ){
            var l = r.split(v);
             
             languagesDropdown.dataSource.add( {text : l[2] +"  " + l[4], voice: l[2] });

        }
        //languagesDropdown.dataSource = ds;
        languagesDropdown.selectedIndex =0;

        hasInitialised = true;
    }

    private function paramsForEspeak() {

        var l:Array<String> = new Array();
        l.push("-v");
        var v:String = languagesDropdown.selectedItem.voice;
        v = variantDropdown.selectedIndex > 0 ? v.split("-")[0]+ "+"+variantDropdown.selectedItem.text : v;
        //v += effects.selectedIndex > 0 ? "+"+effects.text : "" ;
        l.push(v);
        if (ssml.selected){
            l.push("-m");
        }


        l.push("-a");
        l.push("" + volume.value);
        l.push("-p");
        l.push("" + pitch.value);
        l.push("-s");
        l.push("" + speed.value);
        l.push("-g");
        l.push("" + gap.value);
        return l;
    }

    public function play(s:String){

        var l = paramsForEspeak();
        
        l.push(s);
        #if (target.threaded)
        sys.thread.Thread.create(() -> {
            playProcess = new Process(Config.getParameter("type", "espeak"), l);
            playProcess.exitCode(true);
                playProcess.close();
        });
        #end
    }

    @:bind(this,haxe.ui.events.UIEvent.READY)
    public function canWork(e) {
        Config.currentConfigName = this.text; 
        var path = Config.getParameter("path", null);
        warning.hidden = Utils.checkCommand(path);
        warning_text.text = path + " doesn't exist";
        params_box.disabled = !warning.hidden;
    }

    public function stopText(){
        if (playProcess != null) {
            Sys.command("kill " + playProcess.getPid() );
            playProcess.kill();
            playProcess.close();
        }
    }

    public function saveWave(savePath:String = null) {

        var s = rootComponent.findComponent("textBox", haxe.ui.components.TextArea).text;

        var path = Config.getParameter("path", null);
        var cmd = path;
        var l = paramsForEspeak();
        l.push("-w");
        l.push(savePath);
        l.push(s);
        trace(l);
        Sys.command(Config.getParameter("type", "espeak"), l);

        return null;
    }

}