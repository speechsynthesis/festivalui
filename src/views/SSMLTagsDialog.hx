package views;

import haxe.ui.containers.HBox;
import haxe.ui.components.Button;
import haxe.ui.containers.dialogs.Dialog;
import haxe.ui.components.TextArea;
import SSMLTags;

using StringTools;

@:xml('
<vbox width="450">
<label id="description" />
<grid id="grid" width="100%" />
<!--hbox width="450">
<vbox width="20%" id="paramsName">
</vbox>
<vbox width="80%" id="setting">
</vbox>
</hbox-->
</vbox>
')
class SSMLTagsDialog extends Dialog {
	public var tagElement:Xml;
	public var beg:Int = 0;
	public var end = 0;
	public var textBox:TextArea;

	public function new(params:Array<Parameter>, possibleParams:Array<String>) {
		super();

		if (possibleParams == null)
			possibleParams = [];
		buttons = DialogButton.APPLY | DialogButton.CANCEL;
		for (p in params) {
			var pButton = new Button();
			grid.addComponent(pButton);

			switch (p) {
				case Level(s): // 100 est normal
					if (possibleParams.contains(s)) {
						pButton.addClass("highlight");
					}
					pButton.text = s;
					var slider = new haxe.ui.components.HorizontalSlider();
					slider.pos = 100;
					slider.center = 100;
					slider.max = 500;
					slider.percentWidth = 100;
					grid.addComponent(slider);

					slider.onChange = function(e) {
						tagElement.set(s, "" + Std.int(slider.pos));
					}
				case Unit(s, a):
					if (possibleParams.contains(s))
						pButton.addClass("highlight");
					pButton.text = s;
					var bb = new haxe.ui.containers.HorizontalButtonBar();
					var bool = true;
					for (t in a) {
						var button = new Button();
						button.text = t;
						button.toggle = true;
						if (bool) {
							button.selected = true;
							bool = false;
						}
						bb.addComponent(button);
					}

					bb.selectedIndex = 0;

					var ns = new haxe.ui.components.NumberStepper();
					ns.onChange = function(e) {
						var tt = "" + ns.pos + bb.selectedButton.text;

						tagElement.set(s, "" + tt);
					}
					var hbox = new HBox();
					hbox.addComponent(ns);
					hbox.addComponent(bb);
					grid.addComponent(hbox);
				case Option(s, a):
					if (possibleParams.contains(s))
						pButton.addClass("highlight");
					pButton.text = s;

					var dropDown = new haxe.ui.components.DropDown();
					for (o in a) {
						dropDown.dataSource.add(o);
					}
					dropDown.onChange = function(e) {
						var tt = dropDown.selectedItem;

						tagElement.set(s, "" + tt);
					}
					grid.addComponent(dropDown);
				case String(s):
					if (possibleParams.contains(s))
						pButton.addClass("highlight");
					pButton.text = s;
					var textField = new haxe.ui.components.TextField();
					textField.onChange = function(e) {
						tagElement.set(s, "" + textField.text);
					}
					grid.addComponent(textField);

				default:
			}
		}
	}

	@:bind(this, haxe.ui.containers.dialogs.DialogEvent.DIALOG_CLOSED)
	private function validateParam(e:haxe.ui.containers.dialogs.DialogEvent) {
		if (e.button == DialogButton.APPLY)
			textBox.text = textBox.text.substring(0, beg) + tagElement + textBox.text.substring(end);
	}
}
