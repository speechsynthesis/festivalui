package views;

import haxe.ui.containers.VBox;
import haxe.ui.events.UIEvent;

@:build(haxe.ui.ComponentBuilder.build("assets/layout/main-view.xml"))
class MainView extends VBox {

	@:bind(language, UIEvent.READY)
	private function initLanguage(e) {
		language.text = haxe.ui.locale.LocaleManager.instance.language.toUpperCase();
	}

	@:bind(language, UIEvent.CHANGE)
	private function changeLanguage(e) {
		haxe.ui.locale.LocaleManager.instance.language = language.text.toLowerCase();
	}

	public function new() {
        super();
		var i = 0;

		for (s in Config.getConfigurationNames()) {
			var type = Config.toml.getSectionByName(s).getValue().get("type");
			switch (type) {
				case "festival":
					var f = new FestivalView(s, s);
					tabs.addComponent(f);
				case "pico":
					var f = new PicoSayTextComponent(s, s);
					tabs.addComponent(f);
				case "espeak":
					var f = new ESpeakView(s, s);
					tabs.addComponent(f);
				case "mary-tts":
					var m = new MaryTTSView();
					m.text = s;
					m.id = s;
					tabs.addComponent(m);
			}
			i++;
		}   
	}

    @:bind(tabs, UIEvent.READY)
	private function whenReady(e) {
		var tab = cast (tabs.selectedPage, Playable);
        tab.onShow();
	}

	@:bind(tabs, UIEvent.CHANGE)
	private function changeTab(e) {
		var tab = cast (tabs.selectedPage, Playable);
        tab.onShow();
	}
}
