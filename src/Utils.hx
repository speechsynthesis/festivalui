class Utils {
	public static function checkCommand(command:String) {
        if (command == null) return false;
		var path = Config.getParameter("path", null);
		var process = new sys.io.Process("command -v " + command);
		if (process.exitCode() != 0) {
			process.close();
			return false;
		}
		try {
            var l = process.stdout.readAll().toString();
        } catch (e) {
			process.close();
			// this works for hxwidgets
			return false;
		}
		process.close();
		return true;
	}
}
