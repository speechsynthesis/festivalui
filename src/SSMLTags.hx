enum Parameter {
    Level(s:String);  // 100 est normal
    Unit(s:String, unitNames:Array<String>);   //500 ms,, 50 s
    Option(s:String, options:Array<String>);
    String(s:String);
}


class SSMLTags {

    public static final allTags:Map<String, Array<Parameter> >  = [
        "ignore" => null,
        "p"      => null,
        "s"      => null,
        "break"  => [Unit("time", ["s","ms"]), Option("strength", ["none", "x-weak", "weak", "medium", "strong", "x-strong"])],
        "speed"  => [Level("level")],
        "volume" => [Level("level")],
        "emphasis"  =>  [Option("strength", ["none", "strong", "moderate", "reduced"])],
        "sub"  =>  [String("alias")]
    ];

    // distinction option optionnel ?


   // public var phoneme:SSMLTag    =  { tag : "phoneme"  parameters : ["alphabet", "ph"]}//Good evening mister <phoneme alphabet="xsampa" ph="\"br\\a_Un"/> . 
}
