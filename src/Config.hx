import sys.io.File;
import sys.FileSystem;
import toml_parser.TomlParser;

class Config {

    public static var currentConfig:Config = null;
    public static var currentConfigName:String = "";

    public static var mainPlayer = "aplay";

    public static var toml:TOMLSection = null;

    public static function loadConfig(){
        var localConfigPath = haxe.io.Path.directory(Sys.programPath()) + "/config.toml";
        var finalPath = "";
        if (FileSystem.exists(localConfigPath)) finalPath = localConfigPath;
        else {
            if (!FileSystem.exists(Sys.getEnv("HOME") +"/.config")) {
                FileSystem.createDirectory(Sys.getEnv("HOME") +"/.config");
            }
            if (!FileSystem.exists(Sys.getEnv("HOME") +"/.config/ttsui")) {
                FileSystem.createDirectory(Sys.getEnv("HOME") +"/.config/ttsui");
            }
            finalPath = Sys.getEnv("HOME") +"/.config/ttsui/config.toml";
            if (!FileSystem.exists(finalPath)) File.saveContent(finalPath, defaultConfig() );
        }
        var s = sys.io.File.getContent(finalPath);
        toml = new TomlParser().loadFromString(s);

        #if  haxeui_hxwidgets
        var systemLanguage = hx.widgets.Locale.systemLanguage;
        var lang  = hx.widgets.Locale.getLanguageCanonicalName(systemLanguage);

        if (StringTools.startsWith(lang, "fr")) haxe.ui.locale.LocaleManager.instance.language = "fr";
        #end
    }

    public static function getParameter(par:String, defaultP:Dynamic){
        var p = toml.getSectionByName(currentConfigName).getValue().get(par);
        if (p != null)  return p;
        return defaultP;
    }


    public static function getConfigurationNames(){
        var names = toml.getSectionsNames();
        names.remove("General");
        return  names;
    }

    public static function defaultConfig() {
        return '
        [Festival]
type = "festival"
festivalServerPath   = "festival"
festivalClientPath   = "festival_client"
startsFestivalServer = true
#killServerWhenClosing
#server_port = 1234
#server_log_file = "nil"
#server_deny_list =
#server_access_list
#server_passw

[Pico2Wave]
type = "pico"
path = "pico2wave"

[ESpeak]
type = "espeak"
path = "espeak"

[ESpeak-ng]
type = "espeak"
path = "espeak-ng"

[Flite]
type = "flite"
path = "flite"


[Mary-TTS]
type = "mary-tts"
host = "localhost"
port = "59125"
';
    }



    
}