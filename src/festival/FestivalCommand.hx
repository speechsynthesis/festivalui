package festival;

import festival.LispGrammar.*;
import festival.LispGrammar.LispExpression;
import festival.LispGrammar.LispExpression.*;

import haxe.ui.core.Component;


class FestivalCommand {
/*
	public var server_scm = '
	(print "Load server start ./festival_server.scm")
(set! server_port 1235)
(set! server_festival_version "./festival" )
(set! server_log_file "./festival_server.log" )
(set! server_startup_file "./festival_server.scm" )

;; Marks end of machine created bit
;---


 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; The default information below was created by the festival_server script
 ;; You should probably create a file which is similar but with whatever
 ;; access permissions and preloaded voices suit your local situation and
 ;; use the -c flag to festival_server

(defvar server_home ".")

;; Access from machines with no domain name and the local 
(set! server_access_list '("[^.]+" "127.0.0.1" "localhost.*" ))

(cd server_home)
(set! default_access_strategy 'direct)
	
	'*/

	public static function startFestivalServer(){
		var cmd = Config.getParameter("festivalServerPath", "festival") + " --server";

		var serverConfig = "";


		
		
		if (Config.getParameter("server_port", null) != null) serverConfig += "(set! server_port " + 
		Config.getParameter("server_port", null) +")";

		sys.io.File.saveContent("/tmp/serverConfig.scm", serverConfig + "\n");

		#if (target.threaded)
        sys.thread.Thread.create(() -> {
            // lsof -t -i:1314
            // puis kill le process
            Sys.command(cmd + " /tmp/serverConfig.scm");
        });
        #end
	}

	public static function festivalClientCmd(){
		var cmd = Config.getParameter("festivalClientPath", "festival_client");

		if (Config.getParameter("server_port", null) != null) cmd += " --port " +
			Config.getParameter("server_port", null);

		return cmd;
	}

   public static function walkThroughLispExpression(e:LispExpression, a:Array<Int>):LispExpression{

		
		if (a.length == 0 ) return e;


		var pos = a.shift();

		switch (e){
			case LispGrammar.LispExpression.LispList(v): 
				return walkThroughLispExpression(v[pos],a);
			case LispGrammar.LispExpression.LispSymbol(v): return e;
			default: return null;
		}

		return null;


	}

	public static function convertLisp(e:LispExpression):Dynamic{
		
		switch (e){
			case LispGrammar.LispExpression.LispList(v): 
				var a:Array<Dynamic> = new Array();
				for (vv in v ){
					
					a.push(convertLisp(vv));
				}
				return a;
			case LispGrammar.LispExpression.LispSymbol(v): 
                if (v == "nil") return null;
                return v;
			case LispGrammar.LispExpression.LispNumber(v): return v;
			default: return null;
		}


	}

	public static function log(s:String){
		#if haxeui_hxwidgets
		TTSUI.mainView.findComponent(Config.currentConfigName, Component).findComponent("log", haxe.ui.components.TextArea).text += s+"\n";
		#else
		//TTSUI.mainView.findComponent(Config.currentConfigName, Component).findComponent("log", haxe.ui.components.TextArea).dataSource.clear();
		TTSUI.mainView.findComponent(Config.currentConfigName, Component).findComponent("log", haxe.ui.components.TextArea).dataSource.add( s +"\n");
		#end
	}

	public static function sendLispCommand(cmd:String, f: String->Void = null, showLog:Bool = true ){
        if (showLog) {
			var logComponent = TTSUI.mainView.findComponent(Config.currentConfigName, Component).findComponent("log", haxe.ui.components.TextArea);
			if (logComponent == null ) return;
			trace(TTSUI.mainView.findComponent(Config.currentConfigName, Component).findComponent("log", haxe.ui.components.TextArea));
			#if haxeui_hxwidgets
			TTSUI.mainView.findComponent(Config.currentConfigName, Component).findComponent("log", haxe.ui.components.TextArea).text += "$" + cmd +"\n"; 
			#else
			TTSUI.mainView.findComponent(Config.currentConfigName, Component).findComponent("log", haxe.ui.components.TextArea).dataSource.add( "$" + cmd +"\n");//dataSource.add(s);
			#end
		}
		sys.io.File.saveContent("/tmp/myfestivalfile" + Config.currentConfigName +".txt", cmd + "\n");
		var p = new sys.io.Process( festivalClientCmd()+  " /tmp/myfestivalfile" + Config.currentConfigName + ".txt --withlisp");
		p.exitCode();
		var out = p.stdout.readAll().toString();
		var err = p.stdout.readAll().toString();
		if ((f!= null) && (out!= null) && (out!= "")) f(out);
		if (showLog) log(out + err);	
		p.close();
	}

	public static function decodeLispResult(s:String, map:String -> Void){
		var v = LispGrammar.build()(s).value;
		for (l in extractLispExpression(v)){
		switch(l){
			case LispGrammar.LispExpression.LispSymbol(v): map(v);//languagesDropdown.dataSource.add(v);
			default: "";
		}
		}
		
	}

	public static function extractLispExpression (l:LispGrammar.LispExpression){
	switch(l){
		case LispGrammar.LispExpression.LispList(v): return v;
		default : "";
	}
	return null;

	}

    
}