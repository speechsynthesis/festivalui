package ;

import haxe.ui.HaxeUIApp;
import haxe.ui.core.Component;
import haxe.ui.ComponentBuilder;

import views.FestivalView;
import views.PicoSayTextComponent;
import views.ESpeakView;
import views.MaryTTSView;

class TTSUI {

    public static var mainView: Component;

    
    public static function main() {
        Config.loadConfig();
        var app = new HaxeUIApp();
        
        app.ready(function() {       
            HaxeUIApp.instance.icon = "images/TTSIcon.png";  
            mainView = new views.MainView();

            
		    
            
            app.addComponent(mainView);
            app.start();
        });
    }
}
