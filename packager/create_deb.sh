fpm -n TTSUI \
-s dir -t deb \
--name TTSUI \
--license custom \
--version 0.0.2 \
--architecture all \
--description "A GUI for most linux TTS" \
--maintainer "Association Consonne" \
TTSUI.desktop=/usr/share/applications/TTSUI.desktop \
TTSIcon.svg=/usr/share/icons/hicolor/scalable/apps/ttsui.svg \
../build/hxwidgets/TTSUI=/usr/local/bin/TTSUI


