# USE

It adds an UI Interface to multiple TTS systems on Linux.

![](doc/screenshot_dark.png)

## Cloning the repository

```sh
git clone https://gitlab.com/speechsynthesis/festivalui.git

git submodule update --init --recursive
```

# How to run

You need to have a config.toml in the same folder of the app.
You can copy it from the examples folder

# How to build

## Needed libraries

For other libraries, there are one haxelib

`haxe hxwidgets.hxml`
`openfl build linux`

# TODO

Add addenda system, and phonemes system

Flite

- add flite
- add speed
- ssml

Espeak

- add phonemes
- add mode ssml

Festival

- change communication to server so it can stop
- correct command system
- add SABLE
- add phonemes

Pico

- add other pico systems
- add phonemes

MaryTTS

- add options

Spd-say

- add speech dispatcher
